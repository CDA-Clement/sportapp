import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faDumbbell, faRunning,faBicycle,faSwimmer} from '@fortawesome/free-solid-svg-icons';
import { ExerciseDto } from '../dto/exercise-dto';
import { ExerciseRepetitionsDto } from '../dto/exercise-repetitions-dto';
import { TrainingDto } from '../dto/training-dto';
import { ExerciseService } from '../services/exercise.service';
import { TrainingService } from '../services/training.service';


@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  faDumbbell = faDumbbell
  faRunning = faRunning
  faBicycle = faBicycle
  faSwimmer = faSwimmer
  exerciseNames = new Array<string>();
  trainingsOfMonth = new Array<TrainingDto>();
  exercisesOfMonth = new Array<ExerciseDto>();
  exercicesIds= new Array<string>();
  nameRepetions = new Array<ExerciseRepetitionsDto>();
  repetitions = 0;
  newExercises=new Array<ExerciseDto>();
  user: string = localStorage.getItem("current_user") || " ";
  objJson:any;
  todayDate: string;
  choiceOfExerciseToDisplay=""
  idsExerciseAdded: Array<string>;
  newTraining : TrainingDto;
  addedTrainingID: string;
  sportModalToAdd=""
  exerciseToCreate= new ExerciseDto()


  constructor(private exerciseService:ExerciseService,private trainingService:TrainingService, private router: Router) { }
  

  ngOnInit(): void {
    this.todayDate = new Date().toISOString().slice(0,10);
    if(this.user!=" "){
      this.objJson = JSON.parse(this.user);
      this.trainingService.getExerciseName().subscribe(
        data =>{
          this.exerciseNames=data
          this.exerciseNames.forEach(e=>{
            this.newExercises.push(new ExerciseDto(e))
          })
          this.getExercisePeriod()
        }
      )

    
    }else{
    this.router.navigateByUrl("/login")
    }
  }

  getExercisePeriod(){
    this.trainingService.getTrainingsOfPeriod(this.objJson.id,this.todayDate.substring(0,8)+"01",this.todayDate).subscribe(
      data=>{
        this.exercicesIds=[]
        this.trainingsOfMonth = data
         this.trainingsOfMonth.map(t=>{ 
          t.exercises.map(e=>{
            this.exercicesIds.push(e)
          })
        })
        this.trainingService.getExercisesOfPeriod(this.exercicesIds).subscribe(
          data=>{
            this.nameRepetions=[]
            this.repetitions=0
            this.exercisesOfMonth=data
            this.exerciseNames.forEach(name=>{
              this.exercisesOfMonth.filter(e=>e.name == name).forEach(e=>{
                this.repetitions+=e.repetitions
              })
              this.nameRepetions.push(new ExerciseRepetitionsDto(name,this.repetitions))
              this.repetitions=0
            })
          }
        )
      }
    )
  }

  sport(event:string){
    for(var i=1;i<5;i++){
      document.getElementById("icon/"+i).style.color="rgba(0, 0, 0)"
    }
      document.getElementById("icon/"+event.split("/")[0]).style.color="rgba(10, 123, 216, 0.836)"
      this.choiceOfExerciseToDisplay=event.split("/")[1]
  }
  resetModal(){
    for(var i=1;i<5;i++){
      document.getElementById("iconModal/"+i).style.color="rgba(0, 0, 0)"
    }
    this.sportModalToAdd=""
    document.getElementById("modalContent").style.display="none"
    document.getElementById("modalContentAddExercise").style.display="none"
    var form=<HTMLFormElement>document.getElementById("myForm")
      form.reset()
  }
  sportModal(event:string){
    document.getElementById("modalContentAddExercise").style.display="none"
    for(var i=1;i<5;i++){
      document.getElementById("iconModal/"+i).style.color="rgba(0, 0, 0)"
    }
      document.getElementById("iconModal/"+event.split("/")[0]).style.color="rgba(10, 123, 216, 0.836)"
      this.sportModalToAdd=event.split("/")[1]
      document.getElementById("modalContent").style.display="block"
  }
  addExercise(){
    document.getElementById("modalContent").style.display="none"
    document.getElementById("modalContentAddExercise").style.display="block"
  }
  onSubmitAddExercise(){
      this.exerciseToCreate.name=this.exerciseToCreate.name.substring(0,1).toUpperCase()+this.exerciseToCreate.name.substring(1)
      this.exerciseToCreate.repetitions=0
      this.exerciseToCreate.category=this.sportModalToAdd
      this.exerciseService.addExercise(new Array<ExerciseDto>(this.exerciseToCreate)).subscribe(
        data=>{
          var form=<HTMLFormElement>document.getElementById("myFormAddExercise")
          form.reset()
          this.newExercises=[]
          this.trainingService.getExerciseName().subscribe(
            data =>{
              this.exerciseNames=data
              this.exerciseNames.forEach(e=>{
                this.newExercises.push(new ExerciseDto(e))
              })
              this.getExercisePeriod()
            }
          )
          

        }
      )

  }
  onSubmit(){

    this.newExercises.forEach(e=>{
      e.category=this.sportModalToAdd
    })
   this.newExercises = this.newExercises.filter(e=>e.repetitions!=null)
    console.log(this.newExercises)
    this.exerciseService.addExercise(this.newExercises).subscribe(
      data=>{
        this.idsExerciseAdded=data
        console.log(this.idsExerciseAdded)
        this.newTraining=new TrainingDto()
        this.newTraining.category=this.sportModalToAdd
        this.newTraining.profileId=this.objJson.id
        this.newTraining.exercises=this.idsExerciseAdded
        this.addTraining()
      }
    )
  }
  addTraining(){
    this.trainingService.addTraining(this.newTraining).subscribe(
      data=>{
        this.addedTrainingID=data
        console.log("AddTrainiiiiiing")
        console.log(this.addedTrainingID)
      }
      )
      this.getExercisePeriod()
      var form=<HTMLFormElement>document.getElementById("myForm")
      form.reset()
      this.newExercises=[]
      this.trainingService.getExerciseName().subscribe(
        data =>{
          this.exerciseNames=data
          this.exerciseNames.forEach(e=>{
            this.newExercises.push(new ExerciseDto(e))
          })
        }
      )
  }
  
  

}
