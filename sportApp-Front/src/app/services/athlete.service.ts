import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AthleteService {

  allAthletes =  `${environment.backSchema}://${environment.backServer}/athletes/`;
  findAthletes =  `${environment.backSchema}://${environment.backServer}/athlete/`;
  
  refreshContent = new Subject<void>();

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.allAthletes);
  }
}
