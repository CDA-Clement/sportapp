import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ExerciseDto } from '../dto/exercise-dto';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {
  private exercise =  `${environment.backSchema}://${environment.backServer}/exercise/`;
  refreshContent = new Subject<void>();
  constructor(private http: HttpClient) { }
  
  addExercise(newExercises: Array<ExerciseDto>):Observable<any> {
    newExercises.forEach(e=>{
      console.log(e)

    })
    return this.http.post(this.exercise,newExercises);
  }

}
