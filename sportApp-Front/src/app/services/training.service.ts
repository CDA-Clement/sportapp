import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ExerciseDto } from '../dto/exercise-dto';
import { TrainingDto } from '../dto/training-dto';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {
  
  
  private allExerciseNames =  `${environment.backSchema}://${environment.backServer}/training/ExerciseNames`;
  private training =  `${environment.backSchema}://${environment.backServer}/training/`;
  private exercise =  `${environment.backSchema}://${environment.backServer}/exercise/`;
  refreshContent = new Subject<void>();
  constructor(private http: HttpClient) { }

  getExerciseName():Observable<any>{
    return this.http.get(this.allExerciseNames)
  }
  getTrainingsOfPeriod(profileId:string, updatedAtMin:string, updatedAtMax:string):Observable<any>{
    return this.http.get(this.training+"Period/"+profileId+"/"+updatedAtMin+"/"+updatedAtMax)
  }
  getExercisesOfPeriod(profileId:Array<string>):Observable<any>{
    return this.http.get(this.training+"ExercisesPeriod/"+profileId)
  }
  
  addTraining(newTraining: TrainingDto):Observable<any> {
    return this.http.post(this.training,newTraining);
  }
}
