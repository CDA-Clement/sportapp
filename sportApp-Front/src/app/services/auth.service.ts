import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/internal/Subject';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from 'src/environments/environment';
import { RoleDto } from '../dto/role-dto';
import { AthleteDto } from '../dto/athlete-dto';
import { AthleteAuthDto } from '../dto/athlete-auth-dto';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string;
  subjectConnexion: Subject<number>;
  currentUser?: AthleteDto;

  constructor(private router: Router, private http: HttpClient) {
    this.url = `${environment.backSchema}://${environment.backServer}/auth/login`;
    this.subjectConnexion = new Subject<number>();
  }

  isConnected(): boolean {
    return Boolean(localStorage.getItem('isConnected'));
  }

  getCurrentUser(): AthleteDto {
    const userStr = localStorage.getItem('current_user');
    return JSON.parse(userStr||"");
  }

  login(user: AthleteAuthDto): Observable<boolean> {
    return new Observable(observer => {
      this.http.post(this.url, user).subscribe(res => {
      localStorage.setItem('isConnected', 'true');
      localStorage.setItem('access_token', res['token']);
      const currentUser = new AthleteDto();
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(res['token']);
      currentUser.id = decodedToken.sub;
      currentUser.lastName = decodedToken.username;
      currentUser.role = new RoleDto(undefined, decodedToken.roles);
      localStorage.setItem('current_user', JSON.stringify(currentUser));
      this.subjectConnexion.next(3);
        
        observer.next(true);
      },
        err => {
          observer.next(false);
        },
        () => {
          observer.complete();
        });
    });

  }

  logout() {
    localStorage.removeItem('isConnected');
    localStorage.removeItem('access_token');
    localStorage.removeItem('current_user');
    this.subjectConnexion.next(3);
    this.router.navigateByUrl('/login');
  }
}
