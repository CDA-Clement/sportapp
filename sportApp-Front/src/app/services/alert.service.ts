import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AlertDto } from '../dto/alert-dto';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  alertSubject: Subject<AlertDto>;

  constructor(private toastr: ToastrService) {
    this.alertSubject = new Subject<AlertDto>();
  }
  addAnnulation(msg: string) { this.add(msg, 'annulation') }
  addSuccess(msg: string) { this.add(msg, 'success') }
  addDanger(msg: string) { this.add(msg, 'danger') }
  addPrimary(msg: string) { this.add(msg, 'primary') }
  private add(message: string, type: string) {
    if (type === 'success') {
      this.toastr.success(message, 'Succès', {
        closeButton: true,
        positionClass: 'toast-top-right'
      });
    } else if (type === 'danger') {
      this.toastr.error(message, 'Échec', {
        closeButton: true,
      });
    } else if (type === 'primary') {
      this.toastr.info(message, 'Information', {
        closeButton: true,
      
      });
    }else if (type === 'annulation') {
      this.toastr.warning(message, 'succès', {
        closeButton: true,
      
      });
  }
}
}
