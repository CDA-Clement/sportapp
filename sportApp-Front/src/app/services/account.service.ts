import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AthleteDto } from '../dto/athlete-dto';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  accountUrl =  `${environment.backSchema}://${environment.backServer}/account/`;

  
  refreshContent = new Subject<void>();

  constructor(private http: HttpClient) { }

  getMyAccount(id:string): Observable<any> {
    return this.http.get(this.accountUrl+id);
  }
  updateAccount(athlete: AthleteDto):Observable<any>{
    return this.http.put(this.accountUrl,athlete)
  }
}
