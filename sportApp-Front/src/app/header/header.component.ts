import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus, faMinus, faTrash,faBars, faSearch} from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { AlertDto } from '../dto/alert-dto';
import { AlertService } from '../services/alert.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  nbclic:number=0;
  isConnected: boolean;
  isAdmin: boolean;
  faTrash=faTrash
  faSearch = faSearch
  faHam=faBars
  faPlus=faPlus
  faMinus=faMinus
  position: number=0
  posBool: boolean=false
  posBool2: boolean;


  count: number;
  alerts: AlertDto[];
  user = localStorage.getItem("current_user")
  objJson :any;
  @Input() total1:number=-1;
  items:number=0;
  @Input() page=0
  total:number=0;
  @Output()parent=new EventEmitter<any>()
  url:String=environment.backSchema+"://"+environment.backServer
  bool=false

  constructor(private router: Router, private authService: AuthService,private alertService: AlertService) { }

  ngOnInit(): void {
    this.alerts = [];
    this.alertService.alertSubject.subscribe(
      res => {
        this.alerts.push(res);
        setTimeout(() => {
          res.closed = true;
          this.alerts.splice(this.alerts.indexOf(res), 1);
        }, 2000);
      }
    );
    // this.reloadData();
  }
  track(event){
  
    // this.position=parseInt(event.path[1].window.scrollY)
    // var top = this.position+320  
    // document.getElementById('panierco').style.top=top.toString()+'px'
    // if(this.position>=180){
    //   top = this.position+100  
    //   document.getElementById('panierco').style.top=top.toString()+'px'
    //  this.posBool=true 
    //  document.getElementById("navbar").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
    //  document.getElementById("navbar").style.backgroundColor='rgba(255, 196, 0, 0.65)'
    //  document.getElementById("navbar").style.backgroundColor='rgba(255, 196, 0, 0.65)'
    //  document.getElementById("li").style.color='black'
    //  document.getElementById("li1").style.color='black'
    //  document.getElementById("li2").style.color='black'
     
    //  var img=<HTMLImageElement>document.getElementById("img")
    //  img.width=70
    //  img.height=60
    // }
    //    if(this.posBool){
    //     this.countM()
    //    }
   
    //  if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
    //   top = this.position+320 
    //   document.getElementById('panierco').style.top=top.toString()+'px'
    //    this.posBool2=true
    //    this.posBool=true
    //    this.count=0
    //    document.getElementById("navbar").style.boxShadow= "none"
    //    document.getElementById("navbar").style.height='auto'
    //    document.getElementById("navbar").style.backgroundColor='rgb(0,0,0)'
    //    document.getElementById("li").style.color='white'
    //    document.getElementById("li1").style.color='white'
    //    document.getElementById("li2").style.color='white'
      
    //  var img=<HTMLImageElement>document.getElementById("img")
    //  img.width=0
    //  img.height=0 
    //  }
     
  }
  countM(){
    this.count++
    if(this.count>20){   
      this.posBool=false
    }
  }
  logout() {
    console.log("logout")
    localStorage.clear();
    localStorage.setItem('age', '18');
    this.authService.logout()
    

  }
  // reloadData() {
  //   this.isConnected = this.authService.isConnected();
  //   if (this.authService.getCurrentUser()) {
  //     this.isAdmin = this.authService.getCurrentUser().role.roleName === "Admin";
      
  //   }
  //   this.authService.subjectConnexion.subscribe(
  //     res => {
  //       this.isConnected = this.authService.isConnected();
  //       if (this.authService.getCurrentUser()) {
  //         this.isAdmin = this.authService.getCurrentUser().role.roleName === "Admin";
          
  //       }

  //     }
  //   )
  // }

}




  

 

  

 