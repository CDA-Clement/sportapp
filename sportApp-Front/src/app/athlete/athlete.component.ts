import { Component, OnInit } from '@angular/core';
import { AthleteDto } from '../dto/athlete-dto';
import { AthleteService } from '../services/athlete.service';

@Component({
  selector: 'app-athlete',
  templateUrl: './athlete.component.html',
  styleUrls: ['./athlete.component.css']
})
export class AthleteComponent implements OnInit {

  athletes: AthleteDto[] = [];

  constructor(private athleteService:AthleteService) { }

  ngOnInit(): void {
    this.athleteService.getAll().subscribe(
      data=>{
        this.athletes = data
      }
    )

  }

}
