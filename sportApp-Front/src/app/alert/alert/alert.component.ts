import { Component, OnInit, Input } from '@angular/core';
import { AlertDto } from 'src/app/dto/alert-dto';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() alert!: AlertDto;

  constructor() { }

  ngOnInit() {
    // document.getElementById('footerID').style.display="block";

  }
}
