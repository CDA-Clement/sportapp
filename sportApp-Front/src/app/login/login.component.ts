import { Component, OnInit, Inject } from '@angular/core';


import { Router } from '@angular/router';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { AthleteAuthDto } from '../dto/athlete-auth-dto';
import { AlertService } from '../services/alert.service';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  user: AthleteAuthDto = new AthleteAuthDto()
  direction!: string;

  

  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    
    // document.getElementById('margin').style.display = 'non'
    // document.getElementById('id104').style.display = 'none';
    // document.getElementById("general").style.webkitFilter = 'blur(0px)';
    window.scrollTo(0,0)
    this.direction = localStorage.getItem('origine')||"";
    this.user.username = '';
    this.user.password = '';


  }

  login() {
    this.authService.login(this.user).subscribe(res => {
      if (res) {
        localStorage.setItem("loginTime",new Date().getHours().toString())
        this.alertService.addSuccess('bienvenue '+this.user.username);
        // login ok
        if(this.direction!=""){
          this.router.navigateByUrl('/' + this.direction)

        }else{
          this.router.navigateByUrl('/account')
        }

      }
    });
  }

  modalLoginClose() {
    // document.getElementById("general").style.webkitFilter = 'blur(17px)'
    if(this.direction!=undefined){
      this.router.navigateByUrl('/' + this.direction)

    }else{
      this.router.navigateByUrl('/accueil')
    }
  }
  signUp(){
    this.router.navigateByUrl('/signup')
  }
}