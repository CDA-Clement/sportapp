import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpEventType, HttpHeaderResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private alertService: AlertService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      tap(evt => {

      }),
      catchError((err: any) => {

        if (err instanceof HttpErrorResponse
          && (err.status == 403 || err.status == 401)) {
          if (err.status == 401) {
            this.alertService.addDanger('Identifiant ou mot de passe incorrect');
          }
          if(err.status==403&&err.error.error=='Forbidden'&&err.error.message=="Access Denied"&&err.error.path=="/auth/login"){
            this.alertService.addDanger("Vous n'avez pas encore activé votre compte, vérifiez vos mails")
          }
          this.authService.logout();
        }
        if (err instanceof HttpErrorResponse
          && (err.status == 400)) {
            
          this.alertService.addDanger(err.error);
      }
        return of(err);
      }));

  }

}

