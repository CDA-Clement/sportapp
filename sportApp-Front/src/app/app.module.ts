import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptor/auth-interceptor';
import { environment } from 'src/environments/environment';
import { JwtModule } from '@auth0/angular-jwt';
import { ToastrModule } from 'ngx-toastr';
import { AlertComponent } from './alert/alert/alert.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { AthleteComponent } from './athlete/athlete.component';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TrainingComponent } from './training/training.component';


@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    AthleteComponent,
    LoginComponent,
    AccountComponent,
    HeaderComponent,
    TrainingComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    ToastrModule.forRoot(), // ToastrModule added
    JwtModule.forRoot({
      config: {
        // pour injecter le token dans toutes les requetes
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('access_token');
        },
        // inject le token pour tous ces chemin
        allowedDomains: [ environment.backServer],
        // n'injecte pas le token pour ce chemin
        disallowedRoutes: [ `${environment.backSchema}://${environment.backServer}/auth/login`
        ]
      }
    }),
    NgbAlertModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
