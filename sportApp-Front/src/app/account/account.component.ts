import { Component, Inject, NgZone, OnInit, PLATFORM_ID, Renderer2 } from '@angular/core';
import { AthleteDto } from '../dto/athlete-dto';
import { AccountService } from '../services/account.service';
import { faMale,faFemale, faWeight,faRulerHorizontal,faRulerVertical,faClock} from '@fortawesome/free-solid-svg-icons';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { Router } from '@angular/router';
import { WaistDto } from '../dto/waist-dto';
import { WeightDto } from '../dto/weight-dto';
import { BmiDto } from '../dto/bmi-dto';
import { RfmDto } from '../dto/rfm-dto';

@Component({
  selector: 'app-account',
  templateUrl:'./account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  user: string = localStorage.getItem("current_user") || " ";
  objJson:any;
  athlete: AthleteDto = new AthleteDto()
  waist = new Array<WaistDto>()
  weight = new Array<WeightDto>()
  bmi = new Array<BmiDto>()
  rfm = new Array<RfmDto>()
  age:number | undefined
  faMale=faMale
  faFemale=faFemale
  faClock = faClock
  faWeight=faWeight
  faRulerH=faRulerHorizontal
  faRulerV=faRulerVertical
  colorToHighligth: string;
  minValH: number;
  maxValH: number;
  rfmGender : number=64;
  dateWeight : string
  dateWaist : string
  private chart: am4charts.XYChart;
  

  constructor(private renderer:Renderer2,private router: Router, private accountService: AccountService,@Inject(PLATFORM_ID) private platformId, private zone: NgZone) { }
  
  ngOnInit(): void {
    if(this.user!=" "){
      this.objJson = JSON.parse(this.user);
      console.log(this.objJson)
      this.athlete.waist=this.waist
      this.athlete.weight=this.weight
      this.athlete.rfm=this.rfm
      this.athlete.bmi=this.bmi
      
      this.accountService.getMyAccount(this.objJson.id).subscribe(
        data =>{
          this.athlete = data
          console.log(this.athlete)
          if(this.athlete.gender=="Woman"){
            this.rfmGender=76
          }
          this.dateWaist = new Date(this.athlete.waist[0].createdAt).toISOString().split("T")[0]
          this.dateWeight = new Date(this.athlete.weight[0].createdAt).toISOString().split("T")[0]
          this.age = Math.floor((new Date().getTime() - Date.parse(this.athlete.age?.toString().split('T')[0]||""))/31557600000);
          this.createSpeedometerBMI()
          this.createSpeedometerRFM()
        }
      )
    }else{
      this.router.navigateByUrl('/login');
    } 
  }
  createSpeedometerBMI(){
    am4core.useTheme(am4themes_animated);
  
    // create chart
    let chart = am4core.create("chartBMIdiv", am4charts.GaugeChart);
    chart.logo.disabled = true;
    let axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>()); 
    axis.min = 10;
    axis.max = 50;
    axis.strictMinMax = true;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.disabled = true;
    createGrid(10)
    chart.innerRadius = -120;
    chart.startAngle = -180;
    chart.endAngle = -30;
    createGridColor("rgba(92, 122, 138, 1)",10,18.4)
    createGridColor("rgba(98, 138, 92, 1)",18.5,24.9)
    createGridColor("rgba(183, 186, 151, 1)",25,29.9)
    createGridColor("rgba(147, 135, 98, 1)",30,34.9)
    createGridColor("rgba(161, 127, 114, 1)",35,39.9)
    createGridColor("rgba(129, 86, 86, 1)",40,50)
    this.highlightGridBMI()
    createGridColor(this.colorToHighligth,this.minValH,this.maxValH)
    let hand = chart.hands.push(new am4charts.ClockHand());
    hand.value = this.athlete.bmi[0].bmi;


    function createGrid(value:number) {
      for(var i=0;i<=10;i++){
        let range = axis.axisRanges.create();
        range.value = value;
        range.label.text = value.toString();
        value+=4

      }
    }
    
    function createGridColor(color:string,minVal:number,maxVal:number){
      let range = axis.axisRanges.create();
      range.value = minVal;
      range.endValue = maxVal;
      range.axisFill.fillOpacity = 1;
      range.axisFill.fill = am4core.color(color);
      range.axisFill.zIndex = -1;
    }
  }
  createSpeedometerRFM(){
   
      am4core.useTheme(am4themes_animated);
  
      // create chart
      var chart = am4core.create("chartRFMdiv", am4charts.GaugeChart);
      chart.id="chartRFMdiv"
      chart.logo.disabled = true;
      let axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>()); 
      axis.min = 0;
      axis.max = 35;
      axis.strictMinMax = true;
      axis.renderer.grid.template.disabled = true;
      axis.renderer.labels.template.disabled = true;
      createGrid(0)
      chart.innerRadius = -120;
      chart.startAngle = -180;
      chart.endAngle = -30;
      if(this.athlete.gender=="Man"){
        createGridColor("rgba(92, 122, 138, 1)",0,5.9)
        createGridColor("rgba(98, 138, 92, 1)",6,17)
        //createGridColor("#869362",13.1,17)
        createGridColor("#869362",17.1,19.9)
        //createGridColor("#929362",17.1,19.9)
        createGridColor("rgba(147, 135, 98, 1)",20,22.9)
        createGridColor("rgba(161, 127, 114, 1)",23,25.5)
        createGridColor("rgba(129, 86, 86, 1)",25.6,50)
      }else{
        createGridColor("rgba(92, 122, 138, 1)",0,13)
        createGridColor("rgba(98, 138, 92, 1)",13.1,24)
       // createGridColor("rgba(183, 186, 151, 1)",20.1,24)
        createGridColor("rgba(183, 186, 151, 1)",24.1,27.9)
        createGridColor("rgba(161, 127, 114, 1)",28,31.9)
        createGridColor("rgba(129, 86, 86, 1)",32,50)
      }
      this.highlightGridRFM()
      createGridColor(this.colorToHighligth,this.minValH,this.maxValH)
      let hand = chart.hands.push(new am4charts.ClockHand());
      hand.value = this.athlete.rfm[0].rfm;
  
  
      function createGrid(value:number) {
        for(var i=0;i<=10;i++){
          let range = axis.axisRanges.create();
          range.value = value;
          range.label.text = value.toString();
          value+=4
  
        }
      }
      
      function createGridColor(color:string,minVal:number,maxVal:number){
        let range = axis.axisRanges.create();
        range.value = minVal;
        range.endValue = maxVal;
        range.axisFill.fillOpacity = 1;
        range.axisFill.fill = am4core.color(color);
        range.axisFill.zIndex = -1;
      }

    
   
  }
  highlightGridBMI(){
    if(this.athlete.bmi[0].bmi<18.5) { 
        this.colorToHighligth="#0099e6"
        this.minValH=10
        this.maxValH=18.4

     } 
      if(this.athlete.bmi[0].bmi>18.5&&this.athlete.bmi[0].bmi<=24.9) { 
        this.colorToHighligth="#1fe600"
        this.minValH=18.5
        this.maxValH=24.9

    } 
    if(this.athlete.bmi[0].bmi>25&&this.athlete.bmi[0].bmi<=29.9) { 
    this.colorToHighligth="#f1ff52"
    this.minValH=25
        this.maxValH=29.9

    } 
      if(this.athlete.bmi[0].bmi>30&&this.athlete.bmi[0].bmi<=34.9) { 
        this.colorToHighligth="#f5bc00"
        this.minValH=30
        this.maxValH=34.9

    } 
      if(this.athlete.bmi[0].bmi>35&&this.athlete.bmi[0].bmi<=39.9) { 
        this.colorToHighligth="#ff5314"
        this.minValH=35
        this.maxValH=39.9

    } 
    if(this.athlete.bmi[0].bmi>40) { 
      this.colorToHighligth="#d60000"
      this.minValH=40
      this.maxValH=50 
    }
  }
  highlightGridRFM(){
    if(this.athlete.gender=="Man"){
      if(this.athlete.rfm[0].rfm<5.9) { 
        this.colorToHighligth="#0099e6"
        this.minValH=0
        this.maxValH=5.9
  
      } 
      if(this.athlete.rfm[0].rfm>6&&this.athlete.rfm[0].rfm<=17) {
        this.colorToHighligth="#1fe600"
        this.minValH=6
        this.maxValH=17
  
      } 
      // if(this.athlete.rfm[0].rfm>13.1&&this.athlete.rfm[0].rfm<=17) { 
      //   this.colorToHighligth="#b4f500"
      //   this.minValH=13.1
      //   this.maxValH=17
  
      // } 
      if(this.athlete.rfm[0].rfm>17.1&&this.athlete.rfm[0].rfm<=19.9) { 
        this.colorToHighligth="#b4f500"
        this.minValH=17.1
        this.maxValH=19.9
  
      } 
      if(this.athlete.rfm[0].rfm>20&&this.athlete.rfm[0].rfm<=22.9) { 
        this.colorToHighligth="#f5b800"
        this.minValH=20
        this.maxValH=22.9
  
      }
      
      if(this.athlete.rfm[0].rfm>23&&this.athlete.rfm[0].rfm<=25.9) { 
        this.colorToHighligth="#ff5314"
        this.minValH=23
        this.maxValH=25.5
  
      } 
      if(this.athlete.rfm[0].rfm>25.9) { 
        this.colorToHighligth="#d60000"
        this.minValH=26
        this.maxValH=35 
      }
    }else{
      if(this.athlete.rfm[0].rfm<13) { 
        this.colorToHighligth="#0099e6"
        this.minValH=0
        this.maxValH=13

      } 
      if(this.athlete.rfm[0].rfm>13.1&&this.athlete.rfm[0].rfm<=24) { 
        this.colorToHighligth="#1fe600"
        this.minValH=13.1
        this.maxValH=24

      } 
      // if(this.athlete.rfm[0].rfm>20.1&&this.athlete.rfm[0].rfm<=24) { 
      //   this.colorToHighligth="#f1ff52"
      //   this.minValH=20.1
      //   this.maxValH=24

      // } 
      if(this.athlete.rfm[0].rfm>24.1&&this.athlete.rfm[0].rfm<=27.9) { 
        this.colorToHighligth="#f1ff52"
        this.minValH=24.1
        this.maxValH=27.9

      } 
      if(this.athlete.rfm[0].rfm>28&&this.athlete.rfm[0].rfm<=31.9) { 
        this.colorToHighligth="#ff5314"
        this.minValH=28
        this.maxValH=31.9

      } 
      if(this.athlete.rfm[0].rfm>32) { 
        this.colorToHighligth="#d60000"
        this.minValH=32
        this.maxValH=35 
      }
    }
  }
  editMetricsAthlete(event){
    const editWaistTxt = document.getElementById("edit"+event);
    var div = document.createElement("div");
    // div.className="animated flip infinite"
    // var attr = document.createAttribute("data-aos");
    // attr.value="flip-left"
    // div.attributes.setNamedItem(attr)
    div.id="div"+event
    var form = document.createElement("input")
            form.id="form"+event
            form.className="form-control"
            form.setAttribute('type',"number");
            switch (event){
              case "Waist":
                form.setAttribute('value',this.athlete.waist[0].waist.toString());
                form.setAttribute('name',"Cm");
                form.addEventListener("keyup", event => {
                  if (event.key=="Enter") {
                    this.eventEdit("Waist/"+form.valueAsNumber);
                  }
                });
              break
              case "Weight":
              form.setAttribute('value',this.athlete.weight[0].weight.toString());
              form.setAttribute('name',"Kg");
              form.addEventListener("keyup", event => {
                if (event.key=="Enter") {
                  this.eventEdit("Weight/"+form.valueAsNumber);
                }
              });
              break
              case "Size":
                form.setAttribute('value',this.athlete.size.toString());
                form.setAttribute('name',"Cm");
                form.addEventListener("keyup", event => {
                  if (event.key=="Enter") {
                    this.eventEdit("Size/"+form.valueAsNumber);
                  }
                });
              break
            }
            div.appendChild(form)
    var button = document.createElement("button")
    button.id='button'+event
    button.style.marginTop="2%"
    button.className="btn"
    button.style.backgroundColor="green"
    
    var valid = document.createTextNode("validate"); 
    button.appendChild(valid)      
    div.appendChild(button)
    
    editWaistTxt.parentNode.replaceChild(div,editWaistTxt)
    this.renderer.listen(button, 'click', () => this.eventEdit(event+"/"+form.valueAsNumber));
    
   
  }
  
  eventEdit(event:string){
    switch(event.split("/")[0]){
      case "Waist":
        this.athlete.waist.unshift(new WaistDto(new Date(new Date().toISOString()),Number.parseFloat(event.split("/")[1])))
        break
        case "Weight":
        this.athlete.weight.unshift(new WeightDto(new Date(new Date().toISOString()),Number.parseFloat(event.split("/")[1])))
        break
        case "Size":
          this.athlete.size=Number.parseFloat(event.split("/")[1])
          break
        }

        console.log(this.athlete)
        this.accountService.updateAccount(this.athlete).subscribe(
          data=>{    
            const div = document.getElementById("div"+event.split("/")[0])
            var span = document.createElement('span')
            span.id='edit'+event.split("/")[0]
            span.innerText="Enter your "+event.split("/")[0]
            span.className='btn'
            span.style.fontSize='small'
            div.parentNode.replaceChild(span,div)
            this.renderer.listen(span, 'click', () => this.editMetricsAthlete(event.split("/")[0]));
            this.accountService.getMyAccount(this.objJson.id).subscribe(
              data=>{
                this.athlete=data
                this.dateWaist = new Date(this.athlete.waist[0].createdAt).toISOString().split("T")[0]
                this.dateWeight = new Date(this.athlete.weight[0].createdAt).toISOString().split("T")[0]
                this.createSpeedometerRFM()
                this.createSpeedometerBMI()
              }
            )
            
          })
        }
  history(event){
    
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create("divHistory", am4charts.XYChart);
    chart.paddingRight = 20;
    let dataChart = [];
    let visits = 40;
    switch (event){
      case "rfm":
        console.log("rfm")
      for (let i = 0; i < this.athlete.rfm.length; i++) {
       // visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
        dataChart.push({ date: new Date(Number.parseInt(this.athlete.rfm[i].createdAt.toString().split("-")[0]), Number.parseInt(this.athlete.rfm[i].createdAt.toString().split("-")[1])-1, Number.parseInt(this.athlete.rfm[i].createdAt.toString().split("-")[2])), name: "name" + i, value: this.athlete.rfm[i].rfm });
      }
      break
      case "bmi":
        console.log("bmi")
        for (let i = 0; i < this.athlete.bmi.length; i++) {
          // visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
           dataChart.push({ date: new Date(Number.parseInt(this.athlete.bmi[i].createdAt.toString().split("-")[0]), Number.parseInt(this.athlete.bmi[i].createdAt.toString().split("-")[1])-1, Number.parseInt(this.athlete.bmi[i].createdAt.toString().split("-")[2])), name: "name" + i, value: this.athlete.bmi[i].bmi });
         }
      break

    }

    chart.data = dataChart;

    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;

    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "value";
    series.tooltipText = "{valueY.value}";

    chart.cursor = new am4charts.XYCursor();

    let scrollbarX = new am4charts.XYChartScrollbar();
    scrollbarX.series.push(series);
    chart.scrollbarX = scrollbarX;

    this.chart = chart;


  }    

}

     


     


   
    
  

