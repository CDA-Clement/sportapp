export class TrainingDto {
id: string;
name: string;
profileId: string;
rounds: number;
typeOfTraining: string;
difficulty: string;
partOfBody: string;
decreasing: boolean;
category: string;
calorie: number;
isDeleted: boolean;
updatedAt: Date;
exercises: Array<string>
constructor(id?: string, name?: string, profileId?: string, rounds?: number, typeOfTraining?: string, difficulty?: string, partOfBody?: string, decreasing?: boolean, category?: string, calorie?: number, isDeleted?: boolean, updatedAt?: Date,exercises?:Array<string>){
this.id =id;
this.name =name;
this.profileId =profileId;
this.rounds =rounds;
this.typeOfTraining =typeOfTraining;
this.difficulty =difficulty;
this.partOfBody =partOfBody;
this.decreasing = decreasing;
this.category =category ;
this.calorie = calorie;
this.isDeleted = isDeleted;
this.updatedAt = updatedAt;
this.exercises = exercises;
    }
}
