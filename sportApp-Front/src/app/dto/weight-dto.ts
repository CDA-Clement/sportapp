export class WeightDto {
    createdAt: Date
    weight: number
    constructor(createdAt?:Date, weight?:number){
        this.createdAt = createdAt
        this.weight = weight
    }
}
