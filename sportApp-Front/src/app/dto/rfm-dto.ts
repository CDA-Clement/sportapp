export class RfmDto {
    createdAt: Date
    rfm: number
    constructor(createdAt?:Date, rfm?:number){
        this.createdAt = createdAt
        this.rfm = rfm
    }
}
