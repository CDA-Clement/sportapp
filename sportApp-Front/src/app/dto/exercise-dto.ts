export class ExerciseDto {
id:string;
name:string;
repetitions:number;
description:string;
typeOfExercise:string;
difficulty:string;
category:string;
muscles:Array<string>;
calorie:number;

constructor(name?:string, id?:string, repetitions?:number, description?:string, typeOfExercise?:string, difficulty?:string, category?:string, muscles?:Array<string>, calorie?:number ){
this.id = id;
this.name = name;
this.repetitions = repetitions;
this.description = description;
this.typeOfExercise = typeOfExercise;
this.difficulty = difficulty;
this.category = category;
this.muscles = muscles;
this.calorie = calorie;

    }
}
