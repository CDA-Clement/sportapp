export class AlertDto {
    type?: String;
    message?: String;
    closed?: Boolean;

    constructor(type?: string, message?: string, closed?: boolean) { 
        this.type = type
        this.message = message
        this.closed = closed

    }
}
