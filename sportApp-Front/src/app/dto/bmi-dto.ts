export class BmiDto {
    createdAt: Date
    bmi: number
    constructor(createdAt?:Date, bmi?:number){
        this.createdAt = createdAt
        this.bmi = bmi
    }
}
