export class ExerciseRepetitionsDto {
    name:string
    repetitions:number

    constructor(name:string, repetitions:number){
        this.name=name
        this.repetitions=repetitions
    }
}
