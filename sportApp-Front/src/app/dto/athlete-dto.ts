import { BmiDto } from "./bmi-dto";
import { RfmDto } from "./rfm-dto";
import { RoleDto } from "./role-dto";
import { WaistDto } from "./waist-dto";
import { WeightDto } from "./weight-dto";

export class AthleteDto {
    id?: string;
    lastName?: string;
    firstName?: string;
    age?: Date;
	gender?: string;
	nationality?: string;
	weight?: Array<WeightDto>;
    size?: number;
    waist?:Array<WaistDto>;
    rfm?:Array<RfmDto>;
    bmi?:Array<BmiDto>;
	//private List<String> imagesId;
	userName?: string;
    mailAddress?: string;
	tokenSecret?: string;
	password?: string;
	active?: boolean;
	activated?: boolean;
	createdAt?: Date;
	updatedAt?: Date;
	role?: RoleDto;
//	private Goal goal;
	trainingHistory?: string;
	shoes?: string;
    watch?: string;
    
    constructor(id?: string, gender?: string, lastName?: string, waist?: Array<WaistDto>, rfm?: Array<RfmDto>, bmi?: Array<BmiDto>, firstName?: string, userName?: string, nationality?: string,
        password?: string, mailAddress?: string, weight?: Array<WeightDto>, size?: number, age?: Date, createdAt?: Date, updatedAt?: Date, shoes?: string,
        role?: RoleDto, active?: boolean, watch?: string, trainingHistory?: string) {
        this.id = id;
        this.gender = gender
        this.lastName = lastName;
        this.nationality = nationality;
        this.firstName = firstName;
        this.userName = userName;
        this.weight = weight;
        this.waist = waist;
        this.rfm = rfm;
        this.bmi = bmi;
        this.size = size;
        this.password = password;
        this.mailAddress = mailAddress;
        this.age = age;
        this.role = role;
        this.active = active;
        this.watch = watch;
        this.shoes = shoes;
        this.trainingHistory = trainingHistory;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt

    }

}
