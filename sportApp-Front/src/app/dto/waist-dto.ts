export class WaistDto {
    createdAt: Date
    waist: number
    constructor(createdAt?:Date, waist?:number){
        this.createdAt = createdAt
        this.waist = waist
    }
}
