package com.web.sport.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice

public class GlobalControllerExceptionHandler {
	private final static Logger logger = LogManager.getLogger(GlobalControllerExceptionHandler.class);
	
    @ResponseStatus(value =  HttpStatus.UNAUTHORIZED, reason = "Bad credentials")
    @ExceptionHandler(BadCredentialsException.class)
    public void badCredentials(Exception ex) {
    	logger.error(ex.getMessage(),ex);
    }

}