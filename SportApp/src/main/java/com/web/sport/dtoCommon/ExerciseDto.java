package com.web.sport.dtoCommon;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class ExerciseDto {
	
	
	
	private String id;
	private String name;
	private Integer repetitions;
	//private String description;
	//private String typeOfExercise;
	//private String difficulty;
	private String category;
	//private List<String> muscles;
	private Double calorie;
		

	
	
}
