package com.web.sport.dtoCommon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString


public class GenderDto {
	public enum GenderEnumDto{
		
		Man, Woman, Other
	}
	
	
	private String id;
	private GenderEnumDto genderName;
	
}
