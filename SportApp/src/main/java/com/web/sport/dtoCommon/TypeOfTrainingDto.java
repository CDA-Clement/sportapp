package com.web.sport.dtoCommon;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class TypeOfTrainingDto  {
	public enum TypeOfTrainingEnumDto {
		Cardio, Strengh, CardioAndstrengh,

	}
	

	@Id
	private String id;
	private TypeOfTrainingEnumDto typeOfTrainingName;

}
