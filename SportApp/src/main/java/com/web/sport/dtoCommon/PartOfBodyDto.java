package com.web.sport.dtoCommon;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class PartOfBodyDto {
	 	public enum PartOfBodyEnumDto {
		Lower, Upper, Middle, All, LowerandUpper,LowerandMiddle, UpperandMiddle

	}
	
	@Id
	private String id;
	private PartOfBodyEnumDto partOfBodyName;
}
