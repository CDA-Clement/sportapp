package com.web.sport.dtoCommon;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class TrainingHistoryDto {
	
	
	private String id;
	private String profileId;
	private LocalDateTime updatedAt;
	private List<String> trainings;
		

	
	
}
