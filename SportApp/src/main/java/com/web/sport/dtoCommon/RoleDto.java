package com.web.sport.dtoCommon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class RoleDto {
	public enum RoleEnumDto{
		User, Admin	
	}
	
	private String id;
	private RoleEnumDto roleName;
	
}
