package com.web.sport.dtoCommon;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString


public class WaistDto {
	
	
	private LocalDateTime createdAt;
	private double waist;
	
}
