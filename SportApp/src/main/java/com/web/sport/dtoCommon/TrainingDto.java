package com.web.sport.dtoCommon;
import java.time.LocalDateTime;
import java.util.List;

import com.web.sport.entityMongo.PartOfBodyMongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString

public class TrainingDto  {
	
	

	private String id ;
	private String name;
	private String profileId;
	//private Integer rounds;
	//private String typeOfTraining;
	//private String difficulty;
	//private String partOfBody;
	//private Boolean decreasing;
	private String category;
	private Boolean isDeleted;
	//private Double calorie;
	private LocalDateTime updatedAt;
	private List<String> exercises;
	
	
	
	
	
	
}
