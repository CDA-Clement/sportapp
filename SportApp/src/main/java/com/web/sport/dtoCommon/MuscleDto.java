package com.web.sport.dtoCommon;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
public class MuscleDto {
	
	
	@Id
	private String id;
	private String name;
	private String description;
	private String partOfBody;
	private String function;
	 
	 


}
