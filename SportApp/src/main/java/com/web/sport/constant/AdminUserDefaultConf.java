package com.web.sport.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminUserDefaultConf {
	@Value("${sport.admin.username}")
	private String username;
	
	@Value("${sport.admin.lastname}")
	private String nom;
	
	@Value("${sport.admin.firstname}")
	private String prenom;
	
	@Value("${sport.admin.password}")
	private String password;
}
