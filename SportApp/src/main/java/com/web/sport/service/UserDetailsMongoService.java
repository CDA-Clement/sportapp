package com.web.sport.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.web.sport.daoMongo.AthleteRepositoryMongo;
import com.web.sport.daoMongo.RoleRepositoryMongo;
import com.web.sport.entityMongo.AthleteMongo;
import com.web.sport.security.model.UserSecDto;

@Service
public class UserDetailsMongoService implements UserDetailsService {

	@Autowired
    private AthleteRepositoryMongo athleteRepository;
	@Autowired
    private RoleRepositoryMongo roleRepository;
	
	@Override
	public UserSecDto loadUserByUsername(String username) {

		Objects.requireNonNull(username);
		AthleteMongo athleteE = athleteRepository.findByUserNameLike(username).get(0);
				if(athleteE==null) {
					new UsernameNotFoundException("User not found");
				}
			
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(roleRepository.findByRoleName(athleteE.getRole()).getRoleName().toString()));

		return new UserSecDto(
				athleteE.getId(), 
				athleteE.getUserName(), 
				athleteE.getPassword(), 
				authorities,
				athleteE.getActive(), 
				LocalDate.now(), 
				athleteE.getTokenSecret());
	}

}