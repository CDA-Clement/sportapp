package com.web.sport.service.interf;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoMongo.AthleteMongoDto;

public interface IAthleteMongoService {
	
	public List<AthleteMongoDto> findAllAthletes();

	public List<AthleteMongoDto> findAthleteByUserName(String userName);
	
	public List<AthleteMongoDto> listAthletesByLastName(String name);
	
	public List<AthleteMongoDto> listAthletesByFirstName(String firstName);
	
	public List<AthleteMongoDto> listAthletesByFilters(String gender, Collection<String> nationality, Integer sizeMin, Integer sizeMax, Integer weightMin, Integer weightMax, String ageMin, String ageMax, String shoes);

	public List<AthleteMongoDto> listAthleteByRole(int page,int id); // need to check this method
	
	public Optional<AthleteMongoDto> findAthleteById(String id);
	
	public String addAthlete(AthleteMongoDto athlete);
	
	public String updateAthlete(AthleteMongoDto athlete);
	
	public String deleteAthleteById(String id);
	
	public String addTraining(TrainingDto training, String athleteId);

	public String deleteTrainingFromAthlete(String athleteId, String trainingId);
	
	public void findAllAthleteNeverActivated();

	
	
	

	
}
