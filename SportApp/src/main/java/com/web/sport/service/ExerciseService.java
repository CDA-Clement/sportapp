package com.web.sport.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.sport.daoMongo.ExerciseRepositoryMongo;
import com.web.sport.daoMongo.TrainingHistoryRepositoryMongo;
import com.web.sport.dtoCommon.ExerciseDto;
import com.web.sport.dtoMongo.AthleteMongoDto;
import com.web.sport.entityMongo.ExerciseMongo;
import com.web.sport.service.interf.IExerciseService;

import one.util.streamex.StreamEx;

@Service
public class ExerciseService implements IExerciseService{
	
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private ExerciseRepositoryMongo exerciseRepository;
	@Autowired
	private TrainingHistoryRepositoryMongo trainingHistoryRepository;
	
	@Override
	public List<String> findAllTypeOfExercise() {
		List<ExerciseMongo> listMongo=this.exerciseRepository.findAll();
		List<ExerciseMongo> listDistinct = new ArrayList<ExerciseMongo>();
		listDistinct=StreamEx.of(listMongo)
				  .distinct(ExerciseMongo::getName)
				  .toList();
		
		List<String> listName = new ArrayList<String>();
		listDistinct.forEach(l->{
			listName.add(l.getName());
		});
		

		return listName;
	}
	@Override
	public List<ExerciseDto> findListOfExercisesByIds(List<String> ids) {
		List<ExerciseMongo> listMongo = new ArrayList<ExerciseMongo> ();
		ids.forEach(id->{
			Optional<ExerciseMongo> exercise=this.exerciseRepository.findById(id);
			if(exercise.isPresent()) {
				listMongo.add(exercise.get());				
			}
			
		});
		//List<ExerciseMongo> listMongo=this.exerciseRepository.findByIdIn(ids);
		List<ExerciseDto> listExerciseDto = new ArrayList<ExerciseDto>();
		if(!listMongo.isEmpty()) {
			listExerciseDto = listMongo
					.stream()
					.map(e->this.modelMapper.map(e, ExerciseDto.class))
					.collect(Collectors.toList());
		}
		
		return listExerciseDto;
	}
	@Override
	public String updateExercise(ExerciseDto Exercise) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String deleteExercise(String ExerciseId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public ArrayList<String> addExercises(List<ExerciseDto> exercisesDto) {
		ArrayList<String> reponsesString=new ArrayList<String>();
		exercisesDto.forEach(e->{
			try {
				reponsesString.add(this.exerciseRepository.save(this.modelMapper.map(e, ExerciseMongo.class)).getId());
				
			}catch(Exception exception) {
				
			}
		});
		return reponsesString;
	}
	

	
	

}
