package com.web.sport.service.interf;

import java.util.List;
import java.util.Optional;

import com.web.sport.dtoCommon.TrainingDto;

public interface ITrainingService {
	
	public List<TrainingDto> findAllTrainings();

	public Optional<TrainingDto> findByTrainingId(String id);

	public String updateTraining(TrainingDto training);

	public List<TrainingDto> findTrainingyByProfileIdUpdatedAtBetween(String profileId, String updatedAtMin, String updatedAtMax);	
	
	public String deleteTraining(String TrainingId);

	public String addTraining(TrainingDto trainingDto);


	

	
}
