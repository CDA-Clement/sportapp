package com.web.sport.service;

import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.web.sport.daoMongo.AthleteRepositoryMongo;
import com.web.sport.dtoCommon.BmiDto;
import com.web.sport.dtoCommon.RfmDto;
import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoCommon.TrainingHistoryDto;
import com.web.sport.dtoMongo.AthleteMongoDto;
import com.web.sport.entityMongo.AthleteMongo;
import com.web.sport.service.interf.IAthleteMongoService;

@Service
public class AthleteMongoService implements IAthleteMongoService {
	private static DecimalFormat df = new DecimalFormat("0.00");
	private static LocalDateTime ld1 = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Paris"));
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private AthleteRepositoryMongo athleteRepository;
	
	@Autowired
	private TrainingHistoryMongoService trainingHistoryService;
	@Autowired
	private TrainingService trainingService;
	

	@Override
	public List<AthleteMongoDto> findAllAthletes() {
		
		List<AthleteMongoDto> mylist = this.athleteRepository.findAll()
				.stream()
				.map(e -> this.modelMapper.map(e, AthleteMongoDto.class))
				.collect(Collectors.toList());
		
		mylist.forEach(l->{l.setPassword(""); l.setTokenSecret("");
		});
		return mylist;
	}
																			

	@Override
	public Optional<AthleteMongoDto> findAthleteById(String id) {
		Optional<AthleteMongo> athlete = this.athleteRepository.findById(id);
		Optional<AthleteMongoDto> athleteDtoOpt = Optional.empty();
		if(athlete.isPresent()){
			AthleteMongoDto athleteDto = this.modelMapper.map(athlete.get(),AthleteMongoDto.class);
			athleteDto.setPassword("");
			athleteDto.setTokenSecret("");
			athleteDtoOpt=Optional.of(athleteDto);
		}
		return athleteDtoOpt;
	}
	

	@Override
	public String addAthlete(AthleteMongoDto athleteDto) {
		
		Integer rfmGender=64;
		 if(athleteDto.getGender().equalsIgnoreCase("Woman")) {
			 rfmGender=76;
		 }
		try {
			athleteDto.setPassword(passwordEncoder.encode(athleteDto.getPassword()));
			Double bmi = athleteDto.getWeight().get(0).getWeight()/((athleteDto.getSize()/100)*(athleteDto.getSize()/100));
			String sd=df.format(bmi).replace(",", ".");
			bmi = Double.parseDouble(sd);
			athleteDto.setBmi(new ArrayList<BmiDto>());
			athleteDto.getBmi().add(0, BmiDto.builder().bmi(bmi).createdAt(ld1).build());
		
			Double rfm = rfmGender-(20*(athleteDto.getSize()/athleteDto.getWaist().get(0).getWaist()));
			sd=df.format(rfm).replace(",", ".");
			rfm = Double.parseDouble(sd);
			athleteDto.setRfm(new ArrayList<RfmDto>());
			athleteDto.getRfm().add(0, RfmDto.builder().rfm(rfm).createdAt(ld1).build());
			
			AthleteMongo athlete = athleteRepository.save(this.modelMapper.map(athleteDto, AthleteMongo.class));
			
			String trainingProfileId = this.trainingHistoryService.addTrainingHistory(TrainingHistoryDto.builder()
					.profileId(athlete.getId())
					.trainings(new ArrayList<String>())
					.build());
			if(trainingProfileId.equals(athlete.getId())){
				athlete.setTrainingHistory(this.trainingHistoryService.findTrainingHistoryByProfileId(trainingProfileId).get().getId());
				this.athleteRepository.save(athlete);
			}
	
			return athlete.getUserName();
			
		}catch(Exception e) {
			return e.getMessage();
		}
	}


	@Override
	public String updateAthlete(AthleteMongoDto athleteDto) {
		
		 Integer rfmGender=64;
		 if(athleteDto.getGender().equalsIgnoreCase("Woman")) {
			 rfmGender=76;
		 }
		Optional<AthleteMongo> athleteMongo = this.athleteRepository.findById(athleteDto.getId());
		if(athleteMongo.isEmpty()) {return "not found";}
			try {
				Double bmi = athleteDto.getWeight().get(0).getWeight()/((athleteDto.getSize()/100)*(athleteDto.getSize()/100));
				String sd=df.format(bmi).replace(",", ".");
				bmi = Double.parseDouble(sd);
				if(athleteMongo.get().getBmi().get(0).getCreatedAt().toString().split("T")[0].matches(ld1.toString().split("T")[0])){
					athleteDto.getBmi().set(0, BmiDto.builder().bmi(bmi).createdAt(ld1).build());
				}else {
					athleteDto.getBmi().add(0, BmiDto.builder().bmi(bmi).createdAt(ld1).build());
				}
			
				Double rfm = rfmGender-(20*(athleteDto.getSize()/athleteDto.getWaist().get(0).getWaist()));
				sd=df.format(rfm).replace(",", ".");
				rfm = Double.parseDouble(sd);
				if(athleteMongo.get().getRfm().get(0).getCreatedAt().toString().split("T")[0].matches(ld1.toString().split("T")[0])){
					athleteDto.getRfm().set(0, RfmDto.builder().rfm(rfm).createdAt(ld1).build());
				}else {
					athleteDto.getRfm().add(0, RfmDto.builder().rfm(rfm).createdAt(ld1).build());					
				}
				
				athleteDto.setPassword(athleteMongo.get().getPassword());
				
				this.athleteRepository.save(this.modelMapper.map(athleteDto, AthleteMongo.class));	
				return athleteDto.getId();
			}catch (Exception e) {
				return e.getMessage();
			}
	}
	
	
	@Override
	public String deleteAthleteById(String id) {
		Optional<AthleteMongo> athlete = this.athleteRepository.findById(id);
		if(athlete.isEmpty()){
			return "Athlete not found";
		}
		String isTrainingDeleted = this.trainingHistoryService.deleteTrainingHistory(athlete.get().getTrainingHistory());
		if(isTrainingDeleted.equals(athlete.get().getTrainingHistory())){
			this.athleteRepository.deleteById(id);
			return id;			
		}
		return isTrainingDeleted;
	}


	@Override
	public List<AthleteMongoDto> findAthleteByUserName(String username) {
		List<AthleteMongo> athletes = this.athleteRepository.findByUserNameLike(username);
		if(athletes.size()==0) {
			return new ArrayList<AthleteMongoDto>();
		}
		List<AthleteMongoDto> athletesDto = athletes.stream()
				.map(e -> this.modelMapper.map(e, AthleteMongoDto.class))
				.collect(Collectors.toList());
		
		athletesDto.forEach(l->{l.setPassword(""); l.setTokenSecret("");
		});
		return athletesDto;
	}
	
	
	@Override
	public List<AthleteMongoDto> listAthletesByFilters(String gender, Collection<String> nationality, Integer sizeMin,Integer sizeMax, Integer weightMin, Integer weightMax, String ageMin, String ageMax, String shoes) {
		List<AthleteMongo> athletes = new ArrayList<AthleteMongo>(); 
		
		if(ageMin.equalsIgnoreCase("0")) {
			ageMin="1900-01-01 00:00:00.000";
		}
		if(ageMax.equalsIgnoreCase("0")) {
			ageMax="2900-01-01 00:00:00.000";
		}
		if(sizeMax==0) {
			sizeMax=Integer.MAX_VALUE;
		}
		if(weightMax==0) {
			weightMax=Integer.MAX_VALUE;
		}
		
		
		
		LocalDateTime ld = LocalDateTime.parse(ageMin,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
		Date ageDateMin = Date.from(ld.atZone(ZoneId.of("Europe/Madrid")).toInstant());
		ld = LocalDateTime.parse(ageMax,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
		Date ageDateMax = Date.from(ld.atZone(ZoneId.of("Europe/Madrid")).toInstant());
		
		if(gender.equalsIgnoreCase("0")) {
			gender="";
		}
		if(shoes.equalsIgnoreCase("0")) {
			shoes="";
		}
		
		if(nationality.isEmpty()) {
			athletes = athleteRepository.findByGenderLikeAndSizeBetweenAndWeightBetweenAndAgeBetweenAndShoesLike(gender, sizeMin, sizeMax, weightMin, weightMax, ageDateMin, ageDateMax, shoes);
		}else { 
			athletes = athleteRepository.findByGenderLikeAndNationalityInAndSizeBetweenAndWeightBetweenAndAgeBetweenAndShoesLike(gender, nationality, sizeMin, sizeMax, weightMin, weightMax, ageDateMin, ageDateMax, shoes);
		}
		
		if(athletes.size()>0) {
			List<AthleteMongoDto> athletesDto = athletes.stream()
					.map(e -> this.modelMapper.map(e, AthleteMongoDto.class))
					.collect(Collectors.toList());
			
			athletesDto.forEach(l->{l.setPassword(""); l.setTokenSecret("");
			});
			
			return athletesDto;
		}
		List<AthleteMongoDto> athletesEmpty = new ArrayList<AthleteMongoDto>(); 
		return athletesEmpty;
		
	}
	

	@Override
	public List<AthleteMongoDto> listAthletesByLastName(String lastName) {
		List<AthleteMongo> athletes = this.athleteRepository.findByLastNameLike(lastName);
		if(athletes.size()==0) {
			return new ArrayList<AthleteMongoDto>();
		}
		List<AthleteMongoDto> athletesDto = athletes.stream()
				.map(e -> this.modelMapper.map(e, AthleteMongoDto.class))
				.collect(Collectors.toList());
		
		athletesDto.forEach(l->{l.setPassword(""); l.setTokenSecret("");
		});
		return athletesDto;
	}
	
	
	@Override
	public List<AthleteMongoDto> listAthletesByFirstName(String firstName) {
		List<AthleteMongo> athletes = this.athleteRepository.findByFirstNameLike(firstName);
		if(athletes.size()==0) {
			return new ArrayList<AthleteMongoDto>();
		}
		List<AthleteMongoDto> athletesDto = athletes.stream()
				.map(e -> this.modelMapper.map(e, AthleteMongoDto.class))
				.collect(Collectors.toList());
		
		athletesDto.forEach(l->{l.setPassword(""); l.setTokenSecret("");
		});
		return athletesDto;
	}
	
	@Override
	public List<AthleteMongoDto> listAthleteByRole(int page, int id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void findAllAthleteNeverActivated() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public String addTraining(TrainingDto training, String athleteID) {
		Optional<AthleteMongo> athlete = this.athleteRepository.findById(athleteID);
		if(athlete.isEmpty()) {
			return "Athlete NotFound";
		}
		Optional<TrainingHistoryDto> trainingHistory = this.trainingHistoryService.findByTrainingHistoryId(athlete.get().getTrainingHistory());
		if(trainingHistory.isEmpty()) {
			return "Oops no training history found for "+athlete.get().getUserName();
		}
		
			training.setProfileId(athleteID);
			training.setUpdatedAt(LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Madrid")));
			String trainingID = trainingService.addTraining(training);
		
		trainingHistory.get().getTrainings().add(trainingID);
		
		return trainingHistoryService.updateTrainingHistory(trainingHistory.get());
	}


	@Override
	public String deleteTrainingFromAthlete(String athleteId, String trainingId) {
		Optional<AthleteMongo> athlete = this.athleteRepository.findById(athleteId);
		if(athlete.isEmpty()) {
			return "Athlete NotFound";
		}
		Optional<TrainingHistoryDto> trainingHistoryOpt = this.trainingHistoryService.findByTrainingHistoryId(athlete.get().getTrainingHistory());
		if(trainingHistoryOpt.isEmpty()) {
			return "Oops no training history found for "+athlete.get().getUserName();
		}
		
		TrainingHistoryDto trainingHistory = trainingHistoryOpt.get();
		trainingHistoryService.updateTrainingHistory(trainingHistory);
		
		return trainingService.deleteTraining(trainingId);
	}


	

	
}
