package com.web.sport.service.interf;

import java.util.List;
import java.util.Optional;

import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoCommon.TrainingHistoryDto;
import com.web.sport.dtoMongo.AthleteMongoDto;

public interface ITrainingHistoryMongoService {
	
	public List<TrainingHistoryDto> findAllTrainingsHistory();

	public Optional<TrainingHistoryDto> findByTrainingHistoryId(String id);

	public String updateTrainingHistory(TrainingHistoryDto trainingHistory);

	public Optional<TrainingHistoryDto> findTrainingHistoryByProfileId(String profileId);
	
	public String deleteTrainingHistory(String TrainingHistoryId);

	public String addTrainingHistory(TrainingHistoryDto trainingHistoryDto);
	

	
}
