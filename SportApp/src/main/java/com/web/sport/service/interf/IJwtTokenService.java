package com.web.sport.service.interf;

import org.springframework.security.core.Authentication;
import com.web.sport.security.model.JwtTokens;
import com.web.sport.security.model.UserSecDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

public interface IJwtTokenService {

    JwtTokens createTokens(Authentication authentication);
    String createToken(UserSecDto user);
    String createRefreshToken(UserSecDto user);

    JwtTokens refreshJwtToken(String token);
    Jws<Claims> validateJwtToken(String token);

}
