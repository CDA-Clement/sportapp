package com.web.sport.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.web.sport.daoMongo.TrainingHistoryRepositoryMongo;
import com.web.sport.daoMongo.TrainingRepositoryMongo;
import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoMongo.AthleteMongoDto;
import com.web.sport.entityMongo.AthleteMongo;
import com.web.sport.entityMongo.TrainingMongo;
import com.web.sport.service.interf.ITrainingService;

@Service
public class TrainingService implements ITrainingService{
	
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private TrainingRepositoryMongo trainingRepository;
	@Autowired
	private TrainingHistoryRepositoryMongo trainingHistoryRepository;
	@Override
	public List<TrainingDto> findAllTrainings() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Optional<TrainingDto> findByTrainingId(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String updateTraining(TrainingDto training) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<TrainingDto> findTrainingyByProfileIdUpdatedAtBetween(String profileId, String updatedAtMin, String updatedAtMax) {
		List<TrainingMongo> trainings = new ArrayList<TrainingMongo>(); 
		updatedAtMin+= " 00:00:00.000";
		updatedAtMax+= " 23:59:59.999";
		LocalDateTime ld = LocalDateTime.parse(updatedAtMin,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
		Date updatedAtDateMin = Date.from(ld.atZone(ZoneId.of("Europe/Madrid")).toInstant());
		ld = LocalDateTime.parse(updatedAtMax,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
		Date updatedAtDateMax = Date.from(ld.atZone(ZoneId.of("Europe/Madrid")).toInstant());
		trainings = this.trainingRepository.findByProfileIdAndUpdatedAtBetween(profileId, updatedAtDateMin, updatedAtDateMax);
		
		if(trainings.size()>0) {
			List<TrainingDto> trainingsDto = trainings
					.stream()
					.map(t -> this.modelMapper.map(t, TrainingDto.class))
					.collect(Collectors.toList());
			return trainingsDto;
		}
		List<TrainingDto> trainingsEmpty = new ArrayList<TrainingDto>(); 
		return trainingsEmpty;
	}
	@Override
	public String deleteTraining(String TrainingId) {
		Optional<TrainingMongo> trainingOpt = trainingRepository.findById(TrainingId);
		if(trainingOpt.isEmpty()){
			return "Training not found";
		}
		trainingOpt.get().setIsDeleted(true);
		trainingOpt.get().setUpdatedAt(LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Madrid")));
		return trainingRepository.save(this.modelMapper.map(trainingOpt.get(), TrainingMongo.class)).getId();	
	}
	@Override
	public String addTraining(TrainingDto trainingDto) {
		trainingDto.setIsDeleted(false);
		trainingDto.setUpdatedAt(LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Madrid")));
		return trainingRepository.save(this.modelMapper.map(trainingDto, TrainingMongo.class)).getId();
	}

	
	

}
