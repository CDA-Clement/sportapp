package com.web.sport.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.web.sport.daoMongo.AthleteRepositoryMongo;
import com.web.sport.daoMongo.TrainingHistoryRepositoryMongo;
import com.web.sport.dtoCommon.TrainingHistoryDto;
import com.web.sport.entityMongo.TrainingHistoryMongo;
import com.web.sport.service.interf.ITrainingHistoryMongoService;

@Service
public class TrainingHistoryMongoService implements ITrainingHistoryMongoService{
	
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private AthleteRepositoryMongo athleteRepository;
	@Autowired
	private TrainingHistoryRepositoryMongo trainingHistoryRepository;

	@Override
	public List<TrainingHistoryDto> findAllTrainingsHistory() {
		List<TrainingHistoryDto> listAllAthletes = trainingHistoryRepository.findAll()
				.stream()
				.map(t -> this.modelMapper.map(t, TrainingHistoryDto.class))
				.collect(Collectors.toList());
		return listAllAthletes;
	}

	@Override
	public Optional<TrainingHistoryDto> findByTrainingHistoryId(String historyId) {
		
		Optional<TrainingHistoryMongo> optTrainingHistory = trainingHistoryRepository.findById(historyId);
		Optional<TrainingHistoryDto> trainingHistoryDto = Optional.empty();
		if(optTrainingHistory.isPresent()){
			trainingHistoryDto = Optional.of(this.modelMapper.map(optTrainingHistory.get(), TrainingHistoryDto.class));
		}
		return trainingHistoryDto;
		
	}

	@Override
	public String updateTrainingHistory(TrainingHistoryDto trainingHistory) {
		LocalDateTime ld = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Madrid"));
		Optional<TrainingHistoryMongo> trainingHisto = this.trainingHistoryRepository.findById(trainingHistory.getId());
		if(trainingHisto.isEmpty()) {return "not found";}
			try {
				trainingHistory.setUpdatedAt(ld);
				this.trainingHistoryRepository.save(this.modelMapper.map(trainingHistory, TrainingHistoryMongo.class));	
				return trainingHistory.getProfileId();
			}catch (Exception e) {
				return e.getMessage();
			}
	}

	@Override
	public Optional<TrainingHistoryDto> findTrainingHistoryByProfileId(String profileId) {
		Optional<TrainingHistoryMongo> trainingHistory = this.trainingHistoryRepository.findByProfileId(profileId);
		Optional<TrainingHistoryDto> trainingHistoryDto=Optional.empty();
		if(trainingHistory.isPresent()) {
			trainingHistoryDto = Optional.of(this.modelMapper.map(trainingHistory.get(), TrainingHistoryDto.class));
		}
		return trainingHistoryDto ;
	}

	@Override
	public String addTrainingHistory(TrainingHistoryDto trainingHistoryDto) {
		try {
			TrainingHistoryMongo trainingHistory = this.trainingHistoryRepository.save(this.modelMapper.map(trainingHistoryDto, TrainingHistoryMongo.class));
			return trainingHistory.getProfileId();
			
		}catch(Exception e) {
			return e.getMessage();
		}
	}

	@Override
	public String deleteTrainingHistory(String trainingHistoryId) {
		try{this.trainingHistoryRepository.deleteById(trainingHistoryId);
		return trainingHistoryId;
		}catch(Exception e) {
			return e.getMessage();
		}
	}

	

}
