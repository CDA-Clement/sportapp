package com.web.sport.service.interf;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.web.sport.dtoCommon.ExerciseDto;

public interface IExerciseService {
	
	public List<String> findAllTypeOfExercise();

	public List<ExerciseDto> findListOfExercisesByIds(List<String> ids);

	public String updateExercise(ExerciseDto Exercise);
	
	public String deleteExercise(String ExerciseId);

	public ArrayList<String> addExercises(List<ExerciseDto> exercisesDto);


	
	

	
}
