package com.web.sport.daoMongo;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.AthleteMongo;
import com.web.sport.entityMongo.ExerciseMongo;




@Repository
public interface ExerciseRepositoryMongo extends MongoRepository<ExerciseMongo, String> {

	public List<ExerciseMongo> findByIdIn(Collection<String> ids);
	
	
}