package com.web.sport.daoMongo;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.TrainingHistoryMongo;






@Repository
public interface TrainingHistoryRepositoryMongo extends MongoRepository<TrainingHistoryMongo, String> {

	public Optional<TrainingHistoryMongo> findByProfileId(String profileId);
	public List<TrainingHistoryMongo> findByUpdatedAtBetween(Date ageMin, Date ageMax);
	public List<TrainingHistoryMongo> findByTrainingsIn(Collection<String> trainings);
	
}