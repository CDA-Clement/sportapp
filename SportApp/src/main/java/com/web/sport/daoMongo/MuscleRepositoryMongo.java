package com.web.sport.daoMongo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.MuscleMongo;




@Repository
public interface MuscleRepositoryMongo extends MongoRepository<MuscleMongo, String> {

	public MuscleMongo findByName(String name);
	//public List<MuscleMongo> findByPartOfBody(String partOfBody);
	
}