package com.web.sport.daoMongo;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.AthleteMongo;






@Repository
public interface AthleteRepositoryMongo extends MongoRepository<AthleteMongo, String> {
	

	public List<AthleteMongo> findByLastNameLike(String firstName);
	
	public List<AthleteMongo> findByFirstNameLike(String lastName);
	
	public List<AthleteMongo> findByUserNameLike(String username);
	
	public List<AthleteMongo> findByGenderLike(String gender);
	
	public List<AthleteMongo> findByGenderLikeAndNationalityInAndSizeBetweenAndWeightBetweenAndAgeBetweenAndShoesLike(String gender, Collection<String> nationality, Integer sizeMin, Integer sizeMax, Integer weightMin, Integer weightMax, Date ageMin, Date ageMax, String shoes);
	
	public List<AthleteMongo> findByGenderLikeAndSizeBetweenAndWeightBetweenAndAgeBetweenAndShoesLike(String gender, Integer sizeMin, Integer sizeMax, Integer weightMin, Integer weightMax, Date ageDateMin, Date ageDateMax, String shoes);
	
	
	
}