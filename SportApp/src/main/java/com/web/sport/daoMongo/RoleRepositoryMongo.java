package com.web.sport.daoMongo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.MuscleMongo;
import com.web.sport.entityMongo.RoleMongo;




@Repository
public interface RoleRepositoryMongo extends MongoRepository<RoleMongo, String> {

	public RoleMongo findByRoleName(String name);
//	public List<RoleMongoDB> findByPartOfBody(String partOfBody);
	
}