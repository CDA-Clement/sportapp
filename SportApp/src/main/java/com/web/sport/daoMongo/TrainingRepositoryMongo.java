package com.web.sport.daoMongo;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.TrainingMongo;




@Repository
public interface TrainingRepositoryMongo extends MongoRepository<TrainingMongo, String> {

	public TrainingMongo findByName(String name);
	public List<TrainingMongo> findByDifficulty(String difficulty);
	public List<TrainingMongo> findByProfileIdAndUpdatedAtBetween(String profileId, Date updatedAtMin, Date updatedAtMax);
	
}