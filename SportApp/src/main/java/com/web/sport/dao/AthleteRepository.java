//package com.web.sport.dao;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.PagingAndSortingRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import com.web.sport.entity.Athlete;
//
//
//
//
//@Repository
//public interface AthleteRepository extends PagingAndSortingRepository<Athlete, Integer> {
//
//	//public List<Athlete> findAll();
//	
//	public Page<Athlete> findAll(Pageable firstPageWithTwoElements);
//
//	Optional<Athlete> findAthleteByUsername(String Athletename);
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.role = :id", 
//			  nativeQuery = true)
//	public List<Athlete> AthletesByRole (Pageable firstPageWithFiveElements,int id);
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.role = :id and actif= :active", 
//			  nativeQuery = true)
//	public List<Athlete> AthletesByRoleActif (Pageable firstPageWithFiveElements,int id, Boolean active);
//
//	public Optional<Athlete> findByUsername(String cap);
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.reservation = true", 
//			  nativeQuery = true)
//	public List<Athlete> AthletesAvecReservationActive();
//	
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.actif = :active", 
//			  nativeQuery = true)
//	public List<Athlete> AthletesActive(Pageable firstPageWithFiveElements, Boolean active);
//
//	@Query(value = "SELECT * FROM Athlete u WHERE u.role = :id", 
//			  nativeQuery = true)
//	public List<Athlete> AthletesByRole(Integer id);
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.Athletename LIKE %:nom%",
//			  nativeQuery = true)
//	public List<Athlete>findAllAthleteParNom(@Param(value = "nom") String nom);
//	
//	@Query(value = "SELECT * FROM Athlete u WHERE u.activated = false",
//			  nativeQuery = true)
//	public List<Athlete> AthletesNeverActivated();
//	
//	
////	@Query(value = "SELECT * FROM Athlete u WHERE u.role = :role", 
////			  nativeQuery = true)
////	public Collection<Commande> findByFilter(Pageable firstPageWithFiveElements, int role);
//	
//
//	
//	
//	
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
