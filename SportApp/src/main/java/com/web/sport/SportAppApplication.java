package com.web.sport;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.web.sport.constant.AdminUserDefaultConf;
import com.web.sport.daoMongo.AthleteRepositoryMongo;
import com.web.sport.daoMongo.MuscleRepositoryMongo;
import com.web.sport.daoMongo.RoleRepositoryMongo;
import com.web.sport.daoMongo.TrainingRepositoryMongo;
import com.web.sport.dtoCommon.WeightDto;
import com.web.sport.entityMongo.AthleteMongo;
import com.web.sport.entityMongo.GenderMongo;
import com.web.sport.entityMongo.GenderMongo.GenderEnum;
import com.web.sport.entityMongo.MuscleMongo;
import com.web.sport.entityMongo.PartOfBodyMongo;
import com.web.sport.entityMongo.PartOfBodyMongo.PartOfBodyEnum;
import com.web.sport.entityMongo.RoleMongo;
import com.web.sport.entityMongo.RoleMongo.RoleEnum;
import com.web.sport.entityMongo.TrainingMongo;


@SpringBootApplication
@EnableMongoRepositories
public class SportAppApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(SportAppApplication.class, args);
	}
	
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}
//	@Bean
//    public MongoClient mongo() {
//        ConnectionString connectionString = new ConnectionString("mongodb+srv://clement:cyby9t6z@cluster0.cip9z.mongodb.net/Cluster0?retryWrites=true&w=majority");
//        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
//          .applyConnectionString(connectionString)
//          .build();
//        
//        return MongoClients.create(mongoClientSettings);
//    }
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//        return new MongoTemplate(mongo(),"test");
//    }
//	@Bean
//	public AthleteCustomRepositoryImpl getAthleteCustom() {
//		
//		try {
//			return new AthleteCustomRepositoryImpl(mongoTemplate());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS");
	}
	
	@Bean
	public CommandLineRunner init(
			AdminUserDefaultConf adminUserConf,
			RoleRepositoryMongo roleRepositoryMongo,
			AthleteRepositoryMongo athleteRepositoryMongo,
			MuscleRepositoryMongo muscleRepositoryMongo,
			TrainingRepositoryMongo trainingRepositoryMongo

			
			) {
		return (String... args)->{
		
			if(athleteRepositoryMongo.findAll().size()==0){
				roleRepositoryMongo.save(RoleMongo.builder().roleName(RoleEnum.User).build());
				roleRepositoryMongo.save(RoleMongo.builder().roleName(RoleEnum.Admin).build());
				
				LocalDateTime ld = LocalDateTime.parse("2013-02-15 13:02:16.100",DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
				Date date = Date.from(ld.atZone(ZoneId.of("Europe/Madrid")).toInstant());
				
				LocalDateTime ld1 = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Paris"));
			    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");	
			    
			    athleteRepositoryMongo.deleteAll();
				muscleRepositoryMongo.deleteAll();
				trainingRepositoryMongo.deleteAll();
			
			    muscleRepositoryMongo.save(MuscleMongo.builder()
						.description("front part of the leg")
						.name("quadriceps")
						.function("used when walking, cycling, running")
						.partOfBody(PartOfBodyMongo.builder().partOfBodyName(PartOfBodyEnum.Lower).toString())
						.build());
			    List<MuscleMongo> muscles= new ArrayList<MuscleMongo>();
			    muscles.add(0, muscleRepositoryMongo.findByName("quadriceps"));
			    
//			    trainingRepositoryMongo.save(TrainingMongo.builder()
//						.calorie(500)
//						.difficulty(DifficultyMongo.builder().difficultyName(DifficultyEnum.Low).build())
//						.name("Zeus")
//						.typeOfTraining(TypeOfTrainingMongo.builder().typeOfTrainingName(TypeOfTrainingEnum.Strengh).build())
//						//.muscles(muscles)
//						//.exercises(exercises)
//						.build());
			    List<TrainingMongo> trainingHistory=new ArrayList<TrainingMongo>();
			    trainingHistory.add(0, trainingRepositoryMongo.findByName("Dione"));
			    
			    List<WeightDto> weightHistory=new ArrayList<WeightDto>();
			    weightHistory.add(WeightDto.builder().createdAt(ld).weight(64.0).build());
				
				athleteRepositoryMongo.save(AthleteMongo.builder()
							.lastName(adminUserConf.getNom())
							.firstName(adminUserConf.getPrenom())
							.userName(adminUserConf.getUsername())
							.mailAddress("clement.leuridon@gmail.com")
							.active(true)
							.activated(true)
							.createdAt(ld)
							.password(passwordEncoder.encode(adminUserConf.getPassword()))
							.role(roleRepositoryMongo.findByRoleName("Admin").getRoleName().toString())
						//	.age("1990-11-16T00:00:00Z")
							.gender(GenderMongo.builder().genderName(GenderEnum.Man).toString())
							.nationality("French")
							.size(173.0)
							.weight(weightHistory)
						//	.trainingHistory(trainingHistory)
							.build());
				
			}	   
		};
	}

}
