package com.web.sport.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class Exercice {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXERCICE_SEQ")
	private Integer id;
	private String name;
	//private enum typeOfExercice;
	private Integer diffuclty;
	private Integer category;
	private Integer muscle;
	private Integer calorie;

//	@Column(unique=true)
//	private String username;
//	@Column(unique=true)
//	private String mailAddress;
	
	
	
	
	
}
