//package com.web.sport.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//import org.hibernate.annotations.DynamicInsert;
//import org.hibernate.annotations.DynamicUpdate;
//
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.ToString;
//
//
//@Entity
//@NoArgsConstructor
//@AllArgsConstructor
//@Setter
//@Getter
//@Builder
//@ToString
//@DynamicInsert
//@DynamicUpdate
//public class Athlete {
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATHLETE_SEQ")
//	private Integer id;
//	private String lastname;
//	private String firstname;
//	private Long age;
//	private Integer gender;
//	private String nationality;
//	private Integer weight;
//	private Integer size;
////	private List<Integer> imagesId;
//	
//	@Column(unique=true)
//	private String username;
//	@Column(unique=true)
//	private String mailAddress;
//	
//	private String tokenSecret;
//	private String password;
//	private Boolean actif;
//	private Boolean activated;
//	private Long heureActivation;
//	private Role role;
//	private Integer goal;
////	@Column(unique=false)
////	private List<Integer> trainingHistory;
//	
//	
//	
//	
//	
//	
//}
