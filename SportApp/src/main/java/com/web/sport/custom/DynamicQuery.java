package com.web.sport.custom;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DynamicQuery {
	
	private String lastName;
	private String firstName;
	private LocalDateTime age;
	private String gender;
	private String nationality;
	private Integer weight;
	private Integer size;
	//private List<String> imagesId;
	private String userName;
	private String mailAddress;
	private Boolean active;
	private Boolean activated;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private String role;
	private String goal;
	private String trainingHistory;
	private String shoes;
	private String watch;

}
