package com.web.sport.custom;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.web.sport.entityMongo.AthleteMongo;

public interface IAthleteMongoRepositoryCustom {
	
	List<AthleteMongo> query(DynamicQuery dynamicQuery);

}
