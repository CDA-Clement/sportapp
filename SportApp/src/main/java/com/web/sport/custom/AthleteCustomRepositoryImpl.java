package com.web.sport.custom;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.MongoRegexCreator.MatchMode;
import org.springframework.data.mongodb.core.query.Query;

import com.web.sport.entityMongo.AthleteMongo;

public class AthleteCustomRepositoryImpl implements IAthleteMongoRepositoryCustom {
	 
		private final MongoTemplate mongoTemplate;
	 
		@Autowired
		public AthleteCustomRepositoryImpl(MongoTemplate mongoTemplate) {
			this.mongoTemplate = mongoTemplate;
		}
			@Override
			public List<AthleteMongo> query(DynamicQuery dynamicQuery) {
				final Query query = new Query();
				final List<Criteria> criteria = new ArrayList<>();
				if(dynamicQuery.getGender()!=null) {
					criteria.add(Criteria.where("gender").regex(MongoRegexCreator.INSTANCE.toRegularExpression(
							dynamicQuery.getGender(), MatchMode.REGEX
					)));
				}
				
				if(dynamicQuery.getNationality()!=null) {
					criteria.add(Criteria.where("nationality").regex(MongoRegexCreator.INSTANCE.toRegularExpression(
							dynamicQuery.getGender(), MatchMode.REGEX
					)));
				}
				if(dynamicQuery.getSize()!=null) {
					criteria.add(Criteria.where("size").gte(dynamicQuery.getSize()));
				}
				return mongoTemplate.find(query, AthleteMongo.class);
			}
		
			
		
}
