package com.web.sport.entityMongo;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "trainings")
public class TrainingMongo  {
	

	@Id
	private String id ;
	private String name;
	private String profileId;
	private Integer rounds;
	private String typeOfTraining;
	private String difficulty;
	private String partOfBody;
	private Boolean decreasing;
	private String category;
	private Double calorie;
	private Boolean isDeleted;
	private LocalDateTime updatedAt;
	private List<String> exercises;
	
	
	
	
	
	
}
