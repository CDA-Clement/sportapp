package com.web.sport.entityMongo;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.web.sport.dtoCommon.BmiDto;
import com.web.sport.dtoCommon.RfmDto;
import com.web.sport.dtoCommon.WaistDto;
import com.web.sport.dtoCommon.WeightDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "athletes")
public class AthleteMongo  {
	

	@Id
	private String id;
	private String lastName;
	private String firstName;
	private LocalDateTime age;
	private String gender;
	private String nationality;
	private List<WeightDto> weight;
	private Double size;
	private List <WaistDto> waist;
	private List<BmiDto> bmi; 
	private List<RfmDto> rfm; 
	//private List<String> imagesId;
	private String userName;
	private String mailAddress;
	private String tokenSecret;
	private String password;
	private Boolean active;
	private Boolean activated;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private String role;
//	private Goal goal;
	private String trainingHistory;
	private String shoes;
	private String watch;
	
	
	
	
	
	
	
}
