package com.web.sport.entityMongo;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "exercises")
public class ExerciseMongo {
	
	
	
	@Id
	private String id;
	private String name;
	private Integer repetitions;
	//private String description;
	//private String typeOfExercise;
	//private String difficulty;
	private String category;
	//private List<String> muscles;
	private Double calorie;

	
	
}
