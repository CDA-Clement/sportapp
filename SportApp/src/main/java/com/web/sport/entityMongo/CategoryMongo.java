package com.web.sport.entityMongo;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "categories")
public class CategoryMongo  {
	public enum TypeOfCateoriesEnum {
		Cycling, Runnning, Swimming, Fitness
	}
	

	@Id
	private String id;
	private TypeOfCateoriesEnum categorieName;

}
