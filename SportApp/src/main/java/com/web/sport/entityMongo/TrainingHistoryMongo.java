package com.web.sport.entityMongo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "trainingsHistories")
public class TrainingHistoryMongo {
	
	
	
	@Id
	private String id;
	private String profileId;
	private LocalDateTime updatedAt;
	private List<String> trainings;
		

	
	
}
