package com.web.sport.entityMongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@Document(collection = "type-exercises")
public class TypeOfExerciseMongo {
	 public enum TypeOfExerciseEnum {
		Cardio, Strengh, CardioAndStrengh,
	}
	
	@Id
	private String id;
	private TypeOfExerciseEnum typeOfExerciseName;
	
}
