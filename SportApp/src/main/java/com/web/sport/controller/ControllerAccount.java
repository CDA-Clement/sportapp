package com.web.sport.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.web.sport.constant.AdminUserDefaultConf;
import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoMongo.AthleteMongoDto;
import com.web.sport.service.interf.IAthleteMongoService;
import com.web.sport.service.interf.ITrainingHistoryMongoService;
import com.web.sport.tools.ResponseDto;
import com.web.sport.tools.ResponseStatus;

@RestController
public class ControllerAccount {
	@Autowired
	private AdminUserDefaultConf adminUserDefaultConf;
	@Autowired
	private IAthleteMongoService athleteService;
	
	@Autowired
	private ITrainingHistoryMongoService trainingHistoryService;
	
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@GetMapping(value = "/account/{Id}")
	public ResponseEntity<?> listAthletesByUserName(@PathVariable String Id) {
		Optional<AthleteMongoDto> athlete = this.athleteService.findAthleteById(Id);
		if(athlete.isEmpty()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" No user found")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok().body(athlete.get());
	}
	

	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@PutMapping(value = "/account")
	public ResponseEntity<?> updateAthlete(@RequestBody AthleteMongoDto athlete){
		if(athlete.getId().isEmpty()) {
			return ResponseEntity.badRequest().build();
		}
		String isAdded = athleteService.updateAthlete(athlete);
		if(isAdded==athlete.getId()) {
			return ResponseEntity.ok(ResponseDto.builder()
					.msg(athlete.getUserName().toString()+" has been updated successfully")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@DeleteMapping(value = "/account/{athleteID}")
	public ResponseEntity<?> deleteAthlete(@PathVariable String athleteID){
		String isDeleted = this.athleteService.deleteAthleteById(athleteID);
		if(isDeleted.equals(athleteID)) {
			return ResponseEntity.ok(ResponseDto.builder()
					.status(ResponseStatus.OK)
					.msg(isDeleted)
					.build()) ;
		}
		return ResponseEntity.ok(ResponseDto.builder()
				.status(ResponseStatus.OK)
				.msg(isDeleted)
				.build()) ;
		
	}
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@PostMapping(value = "/account/training/{athleteID}")
	public ResponseEntity<?> addTrainingToAthlete(
			@RequestBody TrainingDto training,
			@PathVariable String athleteID){
		
		String isAdded = athleteService.addTraining(training, athleteID);
		if(isAdded.equals(athleteID)) {
			return ResponseEntity.ok(ResponseDto.builder()
					.msg(training.getName().toString()+" has been added successfully")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok(ResponseDto.builder()
				.msg(isAdded)
				.status(ResponseStatus.KO)
				.build());
		
		
	}
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@DeleteMapping(value = "/account/training/{athleteId}/{trainingId}")
	public ResponseEntity<?> deleteTrainingFromAthlete(
			@PathVariable String trainingId,
			@PathVariable String athleteId){
		
		String isAdded = athleteService.deleteTrainingFromAthlete(athleteId, trainingId);
		if(isAdded.equals(trainingId)) {
			return ResponseEntity.ok(ResponseDto.builder()
					.msg(trainingId+" has been added successfully")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok(ResponseDto.builder()
				.msg(isAdded)
				.status(ResponseStatus.KO)
				.build());
	}
}
