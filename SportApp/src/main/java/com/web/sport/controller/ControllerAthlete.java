package com.web.sport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.web.sport.constant.AdminUserDefaultConf;
import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.dtoMongo.AthleteMongoDto;
import com.web.sport.service.interf.IAthleteMongoService;
import com.web.sport.service.interf.ITrainingHistoryMongoService;
import com.web.sport.tools.ResponseDto;
import com.web.sport.tools.ResponseStatus;

@RestController
public class ControllerAthlete {
	@Autowired
	private AdminUserDefaultConf adminUserDefaultConf;
	@Autowired
	private IAthleteMongoService athleteService;
	
	@Autowired
	private ITrainingHistoryMongoService trainingHistoryService;
	
	@GetMapping(value = "/athletes")
	public ResponseEntity<?> listAllAthletes() {
		return ResponseEntity.ok().body(this.athleteService.findAllAthletes());
	}
	
//	@GetMapping(value = "/athletes/username/{userName}")
//	public ResponseEntity<?> listAthletesByUserName(@PathVariable String userName) {
//		return ResponseEntity.ok().body(this.athleteService.findAthleteByUserName(userName));
//	}
//	
//	@GetMapping(value = "/athletes/lastname/{lastName}")
//	public ResponseEntity<?> listAthletesByLastName(@PathVariable String lastName) {
//		return ResponseEntity.ok().body(this.athleteService.listAthletesByLastName(lastName));
//	}
//	
//	@GetMapping(value = "/athletes/firstname/{firstName}")
//	public ResponseEntity<?> listAthletesByFirstName(@PathVariable String firstName) {
//		return ResponseEntity.ok().body(this.athleteService.listAthletesByFirstName(firstName));
//	}
	
	@GetMapping(value = "/athletes/{gender}/{nationality}/{sizeMin}/{sizeMax}/{weightMin}/{weightMax}/{ageMin}/{ageMax}/{shoes}")
	public ResponseEntity<?> listAthletesByFilters(
			@PathVariable String gender, 
			@PathVariable List<String> nationality, 
			@PathVariable Integer sizeMin,
			@PathVariable Integer sizeMax,
			@PathVariable Integer weightMin,
			@PathVariable Integer weightMax,
			@PathVariable String ageMin,
			@PathVariable String ageMax,
			@PathVariable String shoes
			) {
		if(nationality.get(0).equalsIgnoreCase("\"\"")) {
			nationality.clear();
		}
		return ResponseEntity.ok().body(this.athleteService.listAthletesByFilters(gender, nationality,sizeMin,sizeMax,weightMin,weightMax,ageMin,ageMax,shoes));
	}

//	@PostMapping(value = "/athlete")
//	public ResponseEntity<?> addAthlete(@RequestBody AthleteMongoDto athlete){
//		String isAdded = athleteService.addAthlete(athlete);
//		if(isAdded==athlete.getUserName()) {
//			return ResponseEntity.ok(ResponseDto.builder()
//					.msg(athlete.getUserName().toString()+" has been added successfully")
//					.status(ResponseStatus.OK)
//					.build());
//		}
//		
//		return ResponseEntity.ok(ResponseDto.builder()
//				.msg(isAdded)
//				.status(ResponseStatus.KO)
//				.build());
//		
//		
//	}
}
