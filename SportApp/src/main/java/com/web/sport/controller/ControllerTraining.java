package com.web.sport.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.web.sport.dtoCommon.ExerciseDto;
import com.web.sport.dtoCommon.TrainingDto;
import com.web.sport.service.interf.IExerciseService;
import com.web.sport.service.interf.ITrainingService;
import com.web.sport.tools.ResponseDto;
import com.web.sport.tools.ResponseStatus;


@RestController
public class ControllerTraining {
	@Autowired
	private IExerciseService exerciseService;
	@Autowired
	private ITrainingService trainingService;
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@GetMapping(value = "/training/ExerciseNames")
	public ResponseEntity<?> listExercises() {
		List<String> exercises = this.exerciseService.findAllTypeOfExercise();
		if(exercises.isEmpty()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" No exercises found")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok().body(exercises);
	}
	
	
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@GetMapping(value = "/training/Period/{profileId}/{updatedAtMin}/{updatedAtMax}")
	public ResponseEntity<?> trainingsofPeriod(
			@PathVariable String profileId, 
			@PathVariable String updatedAtMin, 
			@PathVariable String updatedAtMax) {
		List<TrainingDto> trainings = this.trainingService.findTrainingyByProfileIdUpdatedAtBetween(profileId, updatedAtMin, updatedAtMax);
		if(trainings.isEmpty()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" No trainings found for this period")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok().body(trainings);
	}
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@GetMapping(value = "/training/ExercisesPeriod/{exerciseIds}")
	public ResponseEntity<?> exercisesofPeriod(@PathVariable List<String> exerciseIds) {
		List<ExerciseDto> exercises = this.exerciseService.findListOfExercisesByIds(exerciseIds);
		if(exercises.isEmpty()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" No exercises found for this period")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok().body(exercises);
	}
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@PostMapping(value = "/exercise")
	public ResponseEntity<?> addExercises(@RequestBody List<ExerciseDto> exercisesDto) {
		ArrayList<String> reponsesString=new ArrayList<String>();
		reponsesString=this.exerciseService.addExercises(exercisesDto);
		if(reponsesString.size()<exercisesDto.size()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" an error has occured while saving exercises")
					.status(ResponseStatus.OK)
					.build());
		}
		
		return ResponseEntity.ok().body(reponsesString);
	}
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@PostMapping(value = "/training/")
	public ResponseEntity<?> addTraining(@RequestBody TrainingDto trainingDto) {
		String reponseString="";
		reponseString=this.trainingService.addTraining(trainingDto);
		if(reponseString.isEmpty()) {
			return ResponseEntity.ok().body(ResponseDto.builder()
					.msg(" an error has occured while saving training")
					.status(ResponseStatus.OK)
					.build());
		}
		System.err.println("training saved!!!!!");
		return ResponseEntity.ok().body(reponseString);
	}
	
	
	

}
