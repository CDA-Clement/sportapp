package com.web.sport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.sport.security.model.AuthenticationRequest;
import com.web.sport.security.model.JwtTokens;
import com.web.sport.security.model.RefreshRequest;
import com.web.sport.service.interf.IJwtTokenService;

@RestController
@RequestMapping("auth")
public class ControllerAuthentication {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private IJwtTokenService jwtTokenService;

	@PostMapping("/login")
	public ResponseEntity<Object> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
 		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				authenticationRequest.username, authenticationRequest.password);
		Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

		if (authentication != null && authentication.isAuthenticated()) {
			JwtTokens tokens = jwtTokenService.createTokens(authentication);
			return ResponseEntity.ok().body(tokens);
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpStatus.UNAUTHORIZED.getReasonPhrase());
	}

	@PostMapping(value = "/refresh")
	public ResponseEntity<Object> refreshToken(@RequestBody RefreshRequest refreshRequest) {
		try {
			JwtTokens tokens = jwtTokenService.refreshJwtToken(refreshRequest.refreshToken);
			return ResponseEntity.ok().body(tokens);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpStatus.UNAUTHORIZED.getReasonPhrase());
		}
	}
}